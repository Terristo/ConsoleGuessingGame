﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RateSpiel
{
    class Achievements
    {
        static int gamesRow = 0;

        public static void Menu()
        {
            AchieveAchievements();
            Console.WriteLine("Achievements:");
            Console.WriteLine("-------------");
            if (int.Parse(ConfigurationManager.AppSettings["timesPlayed"]) == 1)
            {
                Console.WriteLine("Ich sehe dir gefällt es      abgeschlossen");
            }
            else
            {
                Console.WriteLine("Ich sehe dir gefällt es      nicht abgeschlossen");
            }

            if (int.Parse(ConfigurationManager.AppSettings["wonInARow"]) == 1)
            {
                Console.WriteLine("Meisterrater                abgeschlossen");
            }
            else
            {
                Console.WriteLine("Meisterrater                 nicht abgeschlossen");
            }

            if (int.Parse(ConfigurationManager.AppSettings["closeTheGame"]) == 1)
            {
                Console.WriteLine("Schließe das Spiel richtig   abgeschlossen");
            }
            else
            {
                Console.WriteLine("Schließe das Spiel richtig   nicht abgeschlossen");
            }
            if (int.Parse(ConfigurationManager.AppSettings["openAchievements"]) == 1)
            {
                Console.WriteLine("Öffne die Achievements       abgeschlossen");
            }
            else
            {
                Console.WriteLine("Fahr mal deine H4X0R Skillz zurück du schlingel :P");
            }
            Console.ReadLine();
            Console.Clear();
            Init.Main();

        }

        //Aktiviert ein Achievement 
        public static void AchievementAct(String key)
        {
            int exchange = int.Parse(ConfigurationManager.AppSettings[key]);
            if (exchange == 0)
            {
                exchange++;
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings[key].Value = "1";
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
            }
            else
            {

            }
        }
        
        public static void AchieveEndGame()
        {
            AchievementAct("closeTheGame");
        }
        public static void AchieveAchievements()
        {
            AchievementAct("openAchievements");
        }
       public static void AchieveTotalGame()
        {
            // in Arbeit
            String key = "aOne";
            int change = int.Parse(ConfigurationManager.AppSettings[key]);

            if(change < 20)
            {
                change++;
                String changeString = change.ToString();
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings[key].Value = changeString;
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");

            }
            else
            {
                AchievementAct("timesPlayed");
            }
        }
        
        public static void AchieveWinsRowWin()
        {
          if (gamesRow < 5)
            {
                gamesRow++;
            }
          else
            {
                AchievementAct("wonInRow");
            }
        }
        public static void AchieveWinsRowLose()
        {
            gamesRow = 0;
        }
    }
}
