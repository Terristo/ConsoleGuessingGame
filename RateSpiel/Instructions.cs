﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RateSpiel
{
    class Instructions
    {
        public static void Text()
        {
            Console.WriteLine("Das Ziel des Spiels ist das richtige Bierfaß zu finden.");
            Console.WriteLine("Denn nur in einem Faß befindet sich schönes Büble Weißbier.");
            Console.WriteLine("In den anderen Fäßern befindet sich Öttinger");
            Console.WriteLine("Nun liegt es an dir, das richtige Faß zu finden!");
            Console.WriteLine("Aber sei vorsichtig, der Wirt wird dich ja nach Schwierigkeit vor unermäßliche Aufgaben stellen!");
        }
    }
}
