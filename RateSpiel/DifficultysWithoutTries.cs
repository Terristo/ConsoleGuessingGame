﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RateSpiel
{
    class DifficultysWithoutTries
    {
        //Schwierigkeit Normal

        public static void Normal(int random)
        {
            int tries = 1;
            bool right = false;
            int inputIf;
            String guess;

            do
            {
                    guess = Console.ReadLine();
                    int.TryParse(guess, out inputIf);

                    if (inputIf == random)
                    {
                        Console.WriteLine("Super, du hast das gute Bier nach " + tries + " mal deinen Gaumen benetzen lassen, Prost!");
                        right = true;
                    }
                    else if (inputIf < random)
                    {
                        Console.WriteLine("Ihhhhhh pfui deifel, du hast das Öttinger erwischt!");
                        Console.WriteLine("Es muss wohl ein Bierfaß mit einer größeren Nummer sein.");
                        //Console.WriteLine("Du hast schon" + tries + " Versuche gebraucht");
                        Console.WriteLine("Probiers nochmal:");
                        right = false;
                    }
                    else if (inputIf > random && inputIf != 0)
                    {
                        Console.WriteLine("Ihhhhhh pfui deifel, du hast das Öttinger erwischt!");
                        Console.WriteLine("Es muss wohl ein Bierfaß mit einer kleineren Nummer sein.");
                        //Console.WriteLine("Du hast schon" + tries + " Versuche gebraucht");
                        Console.WriteLine("Probiers nochmal:");
                        right = false;
                    }
                    else
                    {
                        Console.WriteLine("Eingabe ungültig!");
                        Console.WriteLine("Probiers nochmal:");
                        right = false;

                    }
                    tries++;
                
            } while (right == false);
            Achievements.AchieveTotalGame();
            Console.ReadLine();
            Console.Clear();
            Init.Main();
        }


        // Schwierigkeit Schwer

        public static void Hard(int random)
        {
            Random rnd = new Random();
            int tries = 1;
            bool right = false;
            int inputIf;
            String guess;

            do
            {
                    guess = Console.ReadLine();
                    int.TryParse(guess, out inputIf);

                    if (inputIf == random)
                    {
                        Console.WriteLine("Super, du hast das gute Bier nach " + tries + " mal deinen Gaumen benetzen lassen, Prost!");
                        right = true;
                    }
                    else if (inputIf < random)
                    {
                        Console.WriteLine("Ihhhhhh pfui deifel, du hast das Öttinger erwischt!");
                        Console.WriteLine("Es muss wohl ein Bierfaß mit einer größeren Nummer sein.");
                        //Console.WriteLine("Du hast schon" + tries + " Versuche gebraucht");
                        Console.WriteLine("Probiers nochmal:");
                        right = false;
                        int choose = rnd.Next(0, 1);
                        if (choose == 0 && random != 10)
                        {
                            random++;
                        }
                        else if (choose == 1 && random != 1)
                        {
                            random--;
                        }
                        else
                        {
                            random++;
                        }
                        Console.WriteLine("Das Bierfaß wurde um eine Stelle versschoben!");
                    }
                    else if (inputIf > random)
                    {
                        Console.WriteLine("Ihhhhhh pfui deifel, du hast das Öttinger erwischt!");
                        Console.WriteLine("Es muss wohl ein Bierfaß mit einer kleineren Nummer sein.");
                        //Console.WriteLine("Du hast schon" + tries + " Versuche gebraucht");
                        Console.WriteLine("Probiers nochmal:");
                        right = false;
                        int choose = rnd.Next(0, 1);
                        if (choose == 0 && random != 10)
                        {
                            random++;
                        }
                        else if (choose == 1 && random != 1)
                        {
                            random--;
                        }
                        else
                        {
                            random++;
                        }
                        Console.WriteLine("Das Bierfaß wurde um eine Stelle versschoben!");
                    }
                tries++;
            } while (right == false);
            Achievements.AchieveTotalGame();
            Console.ReadLine();
            Console.Clear();
            Init.Main();
        }


        //Schwierigkeit: hardcore

        public static void Hardcore(int random)
        {
            Random rnd = new Random();
            int tries = 1;
            bool right = false;
            int inputIf;
            String guess;

            do
            {
                    guess = Console.ReadLine();
                    int.TryParse(guess, out inputIf);

                    if (inputIf == random)
                    {
                        Console.WriteLine("Super, du hast das gute Bier nach " + tries + " mal deinen Gaumen benetzen lassen, Prost!");
                        right = true;
                    }
                    else if (inputIf < random)
                    {
                        Console.WriteLine("Ihhhhhh pfui deifel, du hast das Öttinger erwischt!");
                        Console.WriteLine("Es muss wohl ein Bierfaß mit einer größeren Nummer sein.");
                        //Console.WriteLine("Du hast schon" + tries + " Versuche gebraucht");
                        Console.WriteLine("Probiers nochmal:");
                        right = false;
                        random = rnd.Next(1, 10);
                        //Console.WriteLine("####################" + random);
                    }
                    else if (inputIf > random && inputIf != 0)
                    {
                        Console.WriteLine("Ihhhhhh pfui deifel, du hast das Öttinger erwischt!");
                        Console.WriteLine("Es muss wohl ein Bierfaß mit einer kleineren Nummer sein.");
                        //Console.WriteLine("Du hast schon" + tries + " Versuche gebraucht");
                        Console.WriteLine("Probiers nochmal:");
                        right = false;
                        random = rnd.Next(1, 10);
                        // Console.WriteLine("####################" + random);
                    }
                    else
                    {
                        Console.WriteLine("Eingabe ungültig!");
                        Console.WriteLine("Probiers nochmal:");
                        right = false;



                    }
                tries++;
            } while (right == false);
            Achievements.AchieveTotalGame();
            Console.ReadLine();
            Console.Clear();
            Init.Main();
        }

    }
}
