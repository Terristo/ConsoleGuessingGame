﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RateSpiel
{
    class Gamestart
    {
        public static void Start()
        {
            Random rnd = new Random();
            int numberToGuess = rnd.Next(1, 10);
            int difficulty = int.Parse(ConfigurationManager.AppSettings["difficulty"]);
            int triesSett = int.Parse(ConfigurationManager.AppSettings["maxTries"]);
            Console.WriteLine("Einstellungen: Schwierigkeit: " + difficulty + " ; Maximale Versuche: " + triesSett);
            Console.WriteLine("Welches der 10 Bierfäßer ist nicht mit Öttinger gefühlt sondern mit schönen Büble Weißbier?");
            Console.WriteLine("Tippe eine Zahl zwischen 1 und 10 und dich zu entscheiden");
            //Console.WriteLine("####################" + numberToGuess);
            if (triesSett == 0)
            {
                //Unbegrenzte Versuche
                switch (difficulty)
                {
                    case 1:
                        DifficultysWithoutTries.Normal(numberToGuess);
                        break;

                    case 2:
                        DifficultysWithoutTries.Hard(numberToGuess);
                        break;

                    case 3:
                        DifficultysWithoutTries.Hardcore(numberToGuess);
                        break;
                }
            }
            else
            {
                //Begrenzte Versuche
                switch (difficulty)
                {
                    case 1:
                        Difficultys.Normal(numberToGuess, triesSett);
                        break;

                    case 2:
                        Difficultys.Hard(numberToGuess, triesSett);
                        break;

                    case 3:
                        Difficultys.Hardcore(numberToGuess, triesSett);
                        break;
                }
            }
        }
    }
}
