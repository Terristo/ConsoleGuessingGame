﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RateSpiel
{
    class Settings
    {
        public static void Setting()
        {
            String input;
            int inputconvert;
            Console.WriteLine("Folgende Einstellungen stehen dir zu verfügung:");
            Console.WriteLine("1    Schwierigkeitsgrad");
            Console.WriteLine("2    Maximale Versuche");

            input = Console.ReadLine();
            Console.Clear();
            int.TryParse(input, out inputconvert);


            // Schwierigkeit
            if (inputconvert == 1)
            {
                String diff;
                int diffconvert;
                Console.WriteLine("Gib die entsprechende Zahl ein die deinem Niveau entspricht:");
                Console.WriteLine("1    Normal");
                Console.WriteLine("2    Schwer");
                Console.WriteLine("3    Hardcore");
                
                diff = Console.ReadLine();
                    int.TryParse(diff, out diffconvert);

                    if (diffconvert <= 3 || diffconvert >= 1)
                    {
                        UpdateSetting("difficulty", diff);
                    }
                    else
                    {
                        Console.WriteLine("Sry Dude, die Eingabe versteh ich nicht");
                        Console.ReadLine();
                    }
            }

            //Maximale Vorschläge
            else if (inputconvert == 2)
            {
                String maxT;
                int maxTconvert;
                Console.WriteLine("Gib die Anzahl deiner möglichen Versuche ein, 0 bedeutet unendlich viele Versuche:");


                maxT = Console.ReadLine();
                if (maxT != "0")
                {
                    int.TryParse(maxT, out maxTconvert);

                    if (maxTconvert > 0)
                    {

                        UpdateSetting("maxTries", maxT);
                    }
                    else
                    {
                        Console.WriteLine("Sry Dude, die Eingabe versteh ich nicht");
                        Console.ReadLine();
                    }
                }
                else
                {
                    UpdateSetting("maxTries", maxT);
                }
            }
            else
            {
                Console.WriteLine("Sry Dude, die Eingabe versteh ich nicht");
                Console.ReadLine();
            }
            Console.Clear();
            Init.Main();
        }

        public static void UpdateSetting(string key, string value)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings[key].Value = value;
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}