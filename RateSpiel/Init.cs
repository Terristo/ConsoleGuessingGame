﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

/* HA:  - Ausgabe von versuchen  j
        - Maximale Anzahl an Versuchen  j
        - Achievements  //im Debugging + Unfertig, siehe ToDo´s
        - DIfficultys (mir orig. Zahl ändern) j
        - Easter Eggs :P // zum Teil vorhanden
        - Story - Beautyfier ("Bierfässer") Oktoberfest!! j
*/

 /*
    Actuall ToDo:
    - Achievements.cs: TotalGamesPlayed int to string
    - Achievements.cs: Won in a Row eine public int Variable Klassen übergreifend
*/
namespace RateSpiel
{
    class Init
    {
        public static void Main()
        {
            String input;
            int inputconvert;
            Topping(" ");
            WriteFullLine(" ");
            WriteFullLine("===== Die Entscheidung über das richtige Bierfaß =====");
            WriteFullLine("anläßlich des Oktoberfests");
            WriteFullLine("Erstellt von Max Riedmiller");
            WriteFullLine("https://gitlab.com/u/Terristo");
            WriteFullLine("-------------------------------------------");
            WriteFullLine("Tippe folgende Zahlen um zu den Menüpunkten zu gelangen:");
            WriteFullLine("1    Spielstart");
            WriteFullLine("2    Einstellungen");
            WriteFullLine("3    Achievements");
            WriteFullLine("4    Anleitung");
            WriteFullLine("5    Spiel verlassen");
            WriteFullLine(" ");
            WriteFullLine(" ");

            input = Console.ReadLine();
            int.TryParse(input, out inputconvert);

            if(inputconvert == 1)
            {
                Console.Clear();
                Gamestart.Start();
            }
            else if(inputconvert == 2)
            {
                Console.Clear();
                Settings.Setting();
            }
            else if(inputconvert == 3)
            {
                Console.Clear();
                Achievements.Menu();
            }
            else if (inputconvert == 4)
            {
                Console.Clear();
                Instructions.Text();
            }
            else if(inputconvert == 5)
            {
                Achievements.AchieveEndGame();
                Environment.Exit(0);
            }
            else
            {
                Console.WriteLine("Sry Dude, die Eingabe versteh ich nicht");
                Console.ReadLine();
                Main();
            }

        }
        static void WriteFullLine(string value)
        {
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine(value.PadRight(Console.WindowWidth - 1)); 

            Console.ResetColor();
        }
        static void Topping(string value)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine(value.PadRight(Console.WindowWidth - 1));
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(value.PadRight(Console.WindowWidth - 1));
            Console.WriteLine(value.PadRight(Console.WindowWidth - 1));

            Console.ResetColor();
        }
    }

}